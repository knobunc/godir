package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"runtime"
	"time"
)

// 1kb bucket size.
const BucketMax = 1024

type LogObject struct {
	Lv          int
	EnableILogs bool
	Silent      bool
	OutFile     string

	bucket bytes.Buffer
	writer io.Writer
}

/*
*=>  HOW TO USE LOGGER
*=>  Use the logger function to create a new logger object.
*=>  `debug := logger(level int, ilogs bool, quiet bool, ofile string)`
*=>  -1 = Disabled. 0 = INFO, 1 = WARN, 2 = ERROR, 3 = Internal Logs. FATAL will trigger at all levels aside from -1.
*=>  Manually set doIL to log ILOGS without changing overall log level. Basically -v flag.
*=>  The --quiet (-q) flag will only print WARNINGs and ERRORS (as well as FATAL messages, which print always.)
 */

// Pass `nil` to oFile to disable file logging
func Logger(level int, sendILogs bool, quiet bool, oFile string) *LogObject {
	l := &LogObject{Lv: level, EnableILogs: sendILogs, Silent: quiet, OutFile: "NO_FILE"}
	if (oFile == "") && (!*opts.Args.Quiet) { // If LogFile was left blank in the config, and we're not quiet.
		l.writer = os.Stdout
	} else if *opts.Args.Quiet { // If quiet is enabled, log to file only.
		l.OutFile = oFile
		l.writer = &l.bucket
	} else { // If ofile does not exist return NO_FILE as ofile.
		l.OutFile = oFile
		l.writer = io.MultiWriter(os.Stdout, &l.bucket)
	}

	return l
}

// Standard log level
func (d *LogObject) Log(msgs ...interface{}) {
	if d.Lv >= 0 { // Only trigger if logging is enabled.
		fmt.Fprint(d.writer, d.Prefix("LOG"))
		for _, msg := range msgs {
			fmt.Fprintf(d.writer, "%+v", msg)
		}
		fmt.Fprintf(d.writer, "\n")
	}
	d.checkFlush()
}

func (d *LogObject) Warn(msgs ...interface{}) {
	if d.Lv >= 1 { // Only trigger if logging is enabled.
		fmt.Fprint(d.writer, d.Prefix("WARN"))
		for _, msg := range msgs {
			fmt.Fprintf(d.writer, "%+v", msg)
		}
		fmt.Fprint(d.writer, "\n")
	}
	d.checkFlush()
}

func (d *LogObject) Error(msgs ...interface{}) {
	if d.Lv >= 2 { // Only trigger if logging is enabled.
		fmt.Fprint(d.writer, d.Prefix("ERR"))
		for _, msg := range msgs {
			fmt.Fprintf(d.writer, "%+v", msg)
		}
		fmt.Fprint(d.writer, "\n")
	}
	d.checkFlush()
}

// Fatal error. Always prints and exits 1.
func (d *LogObject) Fatal(msgs ...interface{}) {
	fmt.Fprint(d.writer, d.Prefix("FATAL"))
	for _, msg := range msgs {
		fmt.Fprintf(d.writer, "%+v", msg)
	}
	fmt.Fprint(d.writer, "\n")
	d.checkFlush()
	os.Exit(1)
}

// Does not exit 1 afterwards. This should not be used, instead, use [ERROR].
func (d *LogObject) Fatals(msgs ...interface{}) {
	fmt.Fprint(d.writer, d.Prefix("FATAL"))
	for _, msg := range msgs {
		fmt.Fprintf(d.writer, "%+v", msg)
	}
	fmt.Fprint(d.writer, "\n")
	d.checkFlush()
}

func (d *LogObject) Ilog(msgs ...interface{}) {
	if (d.Lv >= 3) || (d.EnableILogs == true) { // Only trigger if internal (super debug) is enabled.
		fmt.Fprint(d.writer, d.Prefix("IL"))
		for _, msg := range msgs {
			fmt.Fprintf(d.writer, "%+v", msg)
		}
		fmt.Fprint(d.writer, "\n")
		d.checkFlush()
	}
}

func (d *LogObject) Prefix(bracket string) string {
	return fmt.Sprintf("[%s][%s] ", bracket, time.Now().Format("3:04 PM"))
}

// Flushes buffer if it has grown larger than BucketMax.
// Then it clears the buffer.
func (d *LogObject) checkFlush() {
	if d.bucket.Len() >= BucketMax {
		d.Flush()
	}
}

// Flush : write the logger's internal logfile bucket to the logfile.
func (d *LogObject) Flush() {
	err := AppendFile(d.OutFile, d.bucket.Bytes())
	if err != nil {
		d.Error("Unable to write buffer to log file: ", err)
	}
	d.bucket.Reset()
}

func MemUsage() string {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	return fmt.Sprintf("MEMINFO:Alloc=%vMiB:HEAP=%vMiB:Sys=%vMiB:NumGC=%v", bToMb(m.Alloc), bToMb(m.HeapAlloc), bToMb(m.Sys), m.NumGC)
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
