package main

import (
	"io/ioutil"
	"math/rand"
	"os"

	"strings"
)

// Modifies the sideNav global to be efficient
func GenSidenav(path string, streak int) { // Streak needs to start at 0

	// Max depth check and finish if otherwise.
	// If the streak has surpassed the max depth, abort.
	// If -1, do everything.
	if *opts.Args.MaxSidebarDepth != -1 {
		if streak >= *opts.Args.MaxSidebarDepth {
			return
		}
	}

	console.Ilog("Running dirTree(", path, ", ", streak, ")")
	// Get a list of files and directories in PATH
	files, err := ioutil.ReadDir(path)
	if err != nil {
		console.Error("Error reading contents of ", path, " : ", err)
		return
	}

	// Loop over every file...
	for _, p := range files {
		// Only consider files that are not in the excludes list.
		if ( !InExcludes(p.Name())/* If the filename is not in the excludes list...*/ ) {
			// First, compare config information with any symlinks we may encounter
			// Check for symlinks
			fi, err := os.Lstat(path + "/" + p.Name())
			if err != nil { console.Error(err); continue; }
			if fi.Mode() & os.ModeSymlink == os.ModeSymlink {
				// if is a symlink
				realPath, err := os.Readlink(path + "/" + p.Name())
				if err != nil { console.Error(err); continue; }
				 // if the realpath is not contained within the webroot...
				 // AND if we're jailing
				 // This shouldn't run IF unjail is set to true
				if ( !(strings.HasPrefix(realPath, *opts.Args.Webroot) && !(*opts.Args.Unjail) ) ) {
					// Abort this file.
					continue
				}
			}

			// If we've gotten this far, then it must either be a regular item or a VALID symlink according to the conf
			// Now, let's check to see if it's a directory or not, since that will change what we want to do.
			if p.IsDir() {
				// If it's a directory, we first need to see if it's empty or not, to put the chevron on those that are filled.
				isEmpty := true
				files, err := ioutil.ReadDir(path + "/" + p.Name())
				if err != nil {
					console.Error("Error reading contents of ", path, " : ", err)
					continue
				}
				// If we got something other than zero files, Empty = false
				// Loop through all files in the directory. If at least one of them is a directory, break and set isEmpty to false
				for _, item := range files {
					if item.IsDir() {
						isEmpty = false
						break
					}
				}

				uid := ""
				choices := strings.Split("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", "")
				for i:= 0; i<8; i++ { uid += choices[rand.Intn(len(choices))] }


				// Set up some values real quick
				fullpath := EscapePath(path + "/" + p.Name())
				// if is not empty
				if !isEmpty {
					sideNav += `<li class="pure-menu-item has-children"><label><input type="checkbox" onclick="dropdown(this)" id="collapse_` + uid + `"/></label><a href="$root-step$/` + fullpath + `" class="pure-menu-link">` + p.Name() + `</a>` + `<ul class="pure-menu-list default-hidden" id="` + uid + `">`
					GenSidenav(path + "/" + p.Name(), streak+1)
					sideNav += "</ul>"
				} else {
					sideNav += `<li class="pure-menu-item"><label><input type="checkbox" id="collapse_` + uid + `"/></label><a href="$root-step$/` + fullpath + `" class="pure-menu-link">` + p.Name() + `</a>`
				}
			} // END if p.IsDir()
		} // END if !(StringInSlice)
	} // END for _, p := range files
} // END GenSideNav()